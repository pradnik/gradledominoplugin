package com.pradny.gradle

import lotus.domino.*
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class DominoAPITask extends DefaultTask {
	
	def beforeAction(){
		//println 'init Notes thread'
		NotesThread.sinitThread()
		//println 'init Notes thread done'
	}
	
	def afterAction(){
		NotesThread.stermThread()
		
	}
	
	
	@TaskAction
	def doTaskAction(){
		beforeAction()
		
		notesAction()
		
		afterAction()
	}
}
