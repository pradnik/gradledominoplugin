package com.pradny.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project

class DominoPlugin implements Plugin<Project> {
    
	void apply(Project project) {
		project.extensions.create('domino', DominoPluginExtension)
		
        project.task('buildNSF', type: DominoNSFBuild)
		
		project.task('buildToServer', type: CopyNSFTask) {
			fileName={project.buildNSF.nsfName}
			
		}
		
		project.buildToServer.dependsOn(project.buildNSF)
		
    } 
}
