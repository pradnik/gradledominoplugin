package com.pradny.gradle

import org.gradle.api.GradleException
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Optional;

import lotus.domino.*

class CopyNSFTask extends DominoAPITask {
	@Input
	def fileName
	
	
	def targetServer = {project.domino.dominoServer}
	
	@Optional
	def targetFileName 
	
	
	def notesAction() {
		String intFileName=(fileName instanceof Closure) ? fileName():fileName
		String intTargetFileName=(targetFileName) ? targetFileName:intFileName
		String intTargetServer=(targetServer instanceof Closure) ? targetServer():targetServer
				
		logger.quiet "source fileName $intFileName"
		logger.quiet "server $intTargetServer"
		logger.quiet "target fileName $intTargetFileName"
		
		Session s=NotesFactory.createSession()
		Database db=s.getDatabase('', intFileName)
		
		Database copyDb=db.createCopy(intTargetServer, intTargetFileName)
		if (!db.isOpen()){
			throw new GradleException("Target database could not be opened ($intTargetServer,$intTargetFileName)")
		}
		
	}
}
