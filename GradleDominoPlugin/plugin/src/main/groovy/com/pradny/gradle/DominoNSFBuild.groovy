package com.pradny.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputDirectory;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction

import com.google.common.util.concurrent.RateLimiter.SleepingTicker;

class DominoNSFBuild extends DefaultTask {
	
	
	
	@InputDirectory
	File odpPath=project.projectDir
	
	@Input
	String nsfName=project.name+'.nsf'
	
	@OutputFile
	File getFileName(){
		new File(notesDataDir,nsfName)
	}	
	
	@Optional
	File notesDir = new File('C:\\IBM\\Notes')
	
	@Optional
	File notesDataDir = findNotesDataDir()
		
	@Optional
	Integer maxWait = 600
	
	@Optional
	Boolean disableCloseCheck=false
	
	
	private File userDir=new File(System.getProperty('user.dir'))
	
	final static DESIGNER = 'designer.exe'
	final static String PROCESS_LIST_CMD = 'tasklist.exe /fo csv /nh'
	final static HEADLESS_FILENAME = 'HEADLESS0.log'
	 
	def File findNotesDataDir(){
		File notesIni=new File(notesDir,'notes.ini')
		def dataPath
		notesIni.eachLine {line -> 
			if (line.toLowerCase().startsWith('directory=')){
				dataPath=line.substring(line.lastIndexOf('=')+1)
			}
		}
		new File(dataPath)
		
	}
	
	@TaskAction
	def notesBuild() {
		File headless=new File(userDir,HEADLESS_FILENAME)
		headless.delete()
		
		def projectFilePath = odpPath.path + '\\' + '.project'
		logger.info 'Repository path:' + projectFilePath
		logger.quiet 'Database name:' + nsfName
				
		String params='true,true,'+nsfName+',importandbuild,'+projectFilePath+','+nsfName
		
		def cmd=notesDir.absolutePath + '\\'+ DESIGNER + ' "=' +notesDir.absolutePath+ '\\notes.ini" -RPARAMS -vmargs -Dcom.ibm.designer.cmd="'+params+'"'
		
		logger.debug cmd
		logger.lifecycle 'starting Designer'
		def proc=cmd.execute()
		proc.waitFor() //wait for designer to start
		
		def processListProc=PROCESS_LIST_CMD.execute()
		if (!processListProc.text.contains('notes2.exe')){
			throw new GradleException('notes2.exe process not running. Designer probably did not start correcty')
		}
		
		logger.lifecycle 'waiting for Designer'
		
		Integer counter=0
		def lastLine=0
		processListProc=PROCESS_LIST_CMD.execute()
		
		while (processListProc.text.contains('notes2.exe')) {
			counter++
			sleep(1000)
			processListProc=PROCESS_LIST_CMD.execute()
			logger.debug 'waiting for designer ' + counter
			
			if (logger.debugEnabled){
				//File headless=new File(userDir,HEADLESS_FILENAME)
				headless.eachLine() { line,num ->
					if (num>lastLine){
						logger.debug '[DESIGNER] - ' + line
						lastLine=num
					}
				}
			}
			
			if (counter>maxWait){
				throw new GradleException("Out of time - waiting for ${counter} seconds" )
			}
		}
		
		if (!disableCloseCheck){
			
			if (!headless.text.contains('job done: Closing Userless Designer')) {
				throw new GradleException('Designer probably did not close correcty')
			}
		}
		
		
		if (!fileName.exists()){
			throw new GradleException("NSF file ${fileName} not created")
		}
		logger.lifecycle 'Designer closed'
		
	}
}
